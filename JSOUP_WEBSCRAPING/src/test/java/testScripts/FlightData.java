package testScripts;

public class FlightData {

	String airlineName;
	String airlineCode;
	String deptTime;
	String deptCity;
	String flightDuration;
	String arriavalTime;
	String arriavalCity;
	String price;
	
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getAirlineCode() {
		return airlineCode;
	}
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}
	public String getDeptTime() {
		return deptTime;
	}
	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}
	public String getDeptCity() {
		return deptCity;
	}
	public void setDeptCity(String deptCity) {
		this.deptCity = deptCity;
	}
	public String getFlightDuration() {
		return flightDuration;
	}
	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}
	public String getArriavalTime() {
		return arriavalTime;
	}
	public void setArriavalTime(String arriavalTime) {
		this.arriavalTime = arriavalTime;
	}
	public String getArriavalCity() {
		return arriavalCity;
	}
	public void setArriavalCity(String arriavalCity) {
		this.arriavalCity = arriavalCity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	}

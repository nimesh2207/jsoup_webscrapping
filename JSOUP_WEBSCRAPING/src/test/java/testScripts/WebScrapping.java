package testScripts;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebScrapping {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		readFlightData("BOM", "IXC", "03/06/2020");

	}

	public static void readFlightData(String originCity,String destCity,String deptDate) throws IOException
	{
		ArrayList<FlightData> allFlightData = new ArrayList<FlightData>();
		
		System.setProperty("webdriver.gecko.driver", "./Driver/geckodriver.exe");
		
		String url = "https://www.makemytrip.com/flight/search?itinerary="+originCity+"-"+destCity+"-"+deptDate+"&tripType=O&paxType=A-1_C-0_I-0&intl=false&cabinClass=E";
		
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.get(url);
		
		
		String element_xpath = "//*[@id=\"left-side--wrapper\"]/div[3]";
		
		new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element_xpath)));
		
		for(int lastRecord = 1;lastRecord <= 100;lastRecord++)
		{
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight);");
		}
		
		String fullData = driver.findElement(By.tagName("body")).getAttribute("innerHTML");
		
		driver.quit();
		
		Document document = Jsoup.parse(fullData);
		
		Elements allFlights = document.select(".fli-list.one-way");
		
		for(Element flight : allFlights)
		{
			String airlineCode = flight.select(".fli-code").first().text();
			String airlineName = flight.select(".airways-name").first().text();
			String deptCity = flight.select(".dept-city").first().text();
			String deptTime = flight.select(".dept-time").first().text();
			String flightDuration = flight.select(".fli-duration").first().text();
			String arrivalCity = flight.select(".arrival-city").first().text();
			String arrivalTime = flight.select(".reaching-time").first().text();
			String price = flight.select(".actual-price").first().text();
			
			FlightData fData = new FlightData();
			
			fData.setAirlineCode(airlineCode);
			fData.setAirlineName(airlineName);
			fData.setDeptCity(deptCity);
			fData.setDeptTime(deptTime);
			fData.setFlightDuration(flightDuration);
			fData.setArriavalCity(arrivalCity);
			fData.setArriavalTime(arrivalTime);
			fData.setPrice(price);
			
			allFlightData.add(fData);
			
			//System.out.println( airlineCode +"---"+ airlineName);
			
		}
		String fileName = "./FlightData_" + originCity + "_" + destCity +"_" + deptDate.replace("/", "") +".csv";
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		
		bw.write("AirlineName,AirlineCode,DepartureCity,DepartureTime,FlightDuration,ArrivalCity,ArrivalTime,Price");
		bw.newLine();
		
		Iterator<FlightData> allFlight = allFlightData.iterator();
		
		while(allFlight.hasNext())
		{
			FlightData fdata = allFlight.next();
			
			System.out.println(fdata.getAirlineName() + "," + fdata.getAirlineCode() + "," + fdata.getDeptCity()
					+ "," + fdata.getDeptTime() + "," + fdata.getFlightDuration()+ "," + fdata.getArriavalCity()
					+ "," + fdata.getArriavalTime() + "," + fdata.getPrice().replace("?", "").trim());
			
			bw.write(fdata.getAirlineName() + "," + fdata.getAirlineCode() + "," + fdata.getDeptCity()
					+ "," + fdata.getDeptTime() + "," + fdata.getFlightDuration()+ "," + fdata.getArriavalCity()
					+ "," + fdata.getArriavalTime() + "," + fdata.getPrice().replace("?", "").trim());
			bw.newLine();
		}
		
		bw.flush();
		
		//Close buffered writer
		bw.close();
	}
}
